#!/bin/sh
# The Girocco installation script
# We will OVERWRITE basedir!

set -e

# What Config should we use?
[ -n "$GIROCCO_CONF" ] || GIROCCO_CONF=Girocco::Config
echo "*** Initializing using $GIROCCO_CONF..."

# First run Girocco::Config consistency checks
perl -I. -M$GIROCCO_CONF -e ''

. ./shlib.sh


echo "*** Setting up basedir..."
rm -fr "$cfg_basedir"
mkdir -p "$cfg_basedir"
cp -a Girocco jobd taskd gitweb html jobs toolbox hooks apache.conf shlib.sh "$cfg_basedir"

# Put the correct Config in place
[ "$GIROCCO_CONF" = "Girocco::Config" ] || cp "$(echo "$GIROCCO_CONF" | sed 's#::#/#g; s/$/.pm/')" "$cfg_basedir/Girocco/Config.pm"


echo "*** Preprocessing scripts..."
perl -I. -M$GIROCCO_CONF -i -pe 's/\@basedir\@/"$Girocco::Config::basedir"/g' "$cfg_basedir"/jobs/*.sh "$cfg_basedir"/jobd/*.sh "$cfg_basedir"/taskd/*.sh "$cfg_basedir"/gitweb/*.sh "$cfg_basedir"/shlib.sh "$cfg_basedir"/hooks/*


if [ -n "$cfg_mirror" ]; then
	echo "--- Remember to start $cfg_basedir/taskd/taskd.pl"
fi
if [ -n "$cfg_push" -a "$cfg_permission_control" = "Group" ]; then
	echo "--- Recommended crontab for root:"
	echo "*/2  * * * * /usr/bin/nice -n 18 /root/fixupcheck.sh # adjust frequency based on number of repos"
fi
echo "--- Also remember to either start $cfg_basedir/jobd/jobd.sh, or add this"
echo "--- to the crontab of $cfg_mirror_user (adjust frequency on number of repos):"
echo "*/30 * * * * /usr/bin/nice -n 18 $cfg_basedir/jobd/jobd.sh -q --all-once"


echo "*** Setting up repository root..."
mkdir -p "$cfg_reporoot"
if [ "$cfg_owning_group" ]; then
	chown ."$cfg_owning_group" "$cfg_reporoot" || echo "WARNING: Cannot chgrp $cfg_owning_group $cfg_reporoot"
fi
chmod a+rwx,g+s,o+t "$cfg_reporoot" || echo "WARNING: Cannot chmod $cfg_reporoot properly"


if [ -n "$cfg_chrooted" ]; then
	echo "*** Setting up chroot jail for pushing..."
	if [ "$(id -u)" -eq 0 ]; then
		./jailsetup.sh
	else
		echo "WARNING: Skipping jail setup, not root"
	fi
fi


echo "*** Setting up jail configuration (project database)..."
mkdir -p "$cfg_chroot" "$cfg_chroot/etc"
touch "$cfg_chroot/etc/passwd" "$cfg_chroot/etc/group"
owngroup=""
[ -z "$cfg_owning_group" ] || owngroup=".$cfg_owning_group"
chown "$cfg_cgi_user""$owngroup" "$cfg_chroot/etc" ||
	echo "WARNING: Cannot chown $cfg_cgi_user.$cfg_owning_group the files"
chmod 02775 "$cfg_chroot/etc" || echo "WARNING: Cannot chmod 02775 $cfg_chroot/etc"


echo "*** Setting up gitweb from git.git..."
if [ ! -f git.git/Makefile ]; then
	echo "ERROR: git.git is not checked out! Did you _REALLY_ read INSTALL?" >&2
	exit 1;
fi
mkdir -p "$cfg_webroot" "$cfg_cgiroot"
(cd git.git && make --quiet gitweb/gitweb.cgi && cp gitweb/gitweb.cgi "$cfg_cgiroot" \
	&& cp gitweb/*.png gitweb/*.css gitweb/*.js "$cfg_webroot")


echo "*** Setting up git-browser from git-browser.git..."
if [ ! -f git-browser.git/git-browser.cgi ]; then
	echo "ERROR: git-browser.git is not checked out! Did you _REALLY_ read INSTALL?" >&2
	exit 1;
fi
mkdir -p "$cfg_webroot"/git-browser "$cfg_cgiroot"
(cd git-browser.git && cp git-browser.cgi "$cfg_cgiroot" \
	&& cp -r *.html *.js *.css js.lib/ JSON/ "$cfg_webroot"/git-browser)
ln -sf "$cfg_webroot/git-browser/JSON" "$cfg_cgiroot"
cat >"$cfg_cgiroot"/git-browser.conf <<EOT
gitbin: $cfg_git_bin
warehouse: $cfg_reporoot
EOT
cat >"$cfg_webroot"/git-browser/GitConfig.js <<EOT
cfg_gitweb_url="$cfg_gitweburl/"
cfg_browsercgi_url="$cfg_webadmurl/git-browser.cgi"
EOT


echo "*** Setting up darcs-fast-export from bzr-fastimport.git..."
if [ ! -d bzr-fastimport.git/exporters/darcs/ ]; then
	echo "ERROR: bzr-fastimport.git is not checked out! Did you _REALLY_ read INSTALL?" >&2
	exit 1;
fi
mkdir -p "$cfg_basedir"/bin
cp bzr-fastimport.git/exporters/darcs/darcs-fast-export "$cfg_basedir"/bin


echo "*** Setting up our part of the website..."
mkdir -p "$cfg_webroot" "$cfg_cgiroot"
cp cgi/*.cgi gitweb/gitweb_config.perl "$cfg_cgiroot"
ln -fs "$cfg_basedir"/Girocco "$cfg_cgiroot"
[ -z "$cfg_webreporoot" ] || { rm -f "$cfg_webreporoot" && ln -s "$cfg_reporoot" "$cfg_webreporoot"; }
cp gitweb/indextext.html "$cfg_webroot"
mv "$cfg_basedir"/html/*.css "$cfg_basedir"/html/*.js "$cfg_webroot"
cp mootools.js "$cfg_webroot"
cp htaccess "$cfg_webroot/.htaccess"
cat gitweb/gitweb.css >>"$cfg_webroot"/gitweb.css
