package Girocco::Config;

use strict;
use warnings;


## Basic settings

# Name of the service
our $name = "GiroccoEx";

# Title of the service (as shown in gitweb)
our $title = "Example Girocco Hosting";

# Path to the Git binary to use (you MUST set this, even if to /usr/bin/git!)
our $git_bin = '/usr/bin/git';

# E-mail of the site admin
our $admin = 'admin@example.org';


## Feature knobs

# Enable mirroring mode if true
our $mirror = 1;

# Enable push mode if true
our $push = 1;

# Enable user management if true; this means the interface for registering
# user accounts and uploading SSH keys. This implies full chroot.
our $manage_users = 1;

# Enable the special 'mob' user if set to 'mob'
our $mob = "mob";

# Let users set admin passwords; if false, all password inputs are assumed empty.
# This will make new projects use empty passwords and all operations on them
# unrestricted, but you will be able to do no operations on previously created
# projects you have set a password on.
our $project_passwords = 1;

# How to determine project owner; 'email' adds a form item asking for their
# email contact, 'source' takes realname of owner of source repository if it
# is a local path (and empty string otherwise). 'source' is suitable in case
# the site operates only as mirror of purely local-filesystem repositories.
our $project_owners = 'email';

# Which project fields to make editable, out of 'shortdesc', 'homepage',
# 'README', 'notifymail', 'notifyjson', 'notifycia'. (This is currently
# soft restriction - form fields aren't used, but manually injected values
# *are* used. Submit a patch if that's an issue for you.)
our @project_fields = qw(homepage shortdesc README notifymail notifyjson notifycia);

# Minimal number of seconds to pass between two updates of a project.
our $min_mirror_interval = 3600; # 1 hour

# Minimal number of seconds to pass between two garbage collections of a project.
our $min_gc_interval = 604800; # 1 week


## Paths

# Path where the main chunk of Girocco files will be installed
# This will get COMPLETELY OVERWRITTEN by each make install!!!
our $basedir = '/home/repo/repomgr';

# The repository collection
our $reporoot = "/srv/git";

# The chroot for ssh pushing; location for project database and other run-time
# data even in non-chroot setups
our $chroot = "/home/repo/j";

# The gitweb files web directory (corresponds to $gitwebfiles)
our $webroot = "/home/repo/WWW";

# The CGI-enabled web directory (corresponds to $gitweburl and $webadmurl)
our $cgiroot = "/home/repo/WWW";

# A web-accessible symlink to $reporoot (corresponds to $httppullurl, can be undef)
our $webreporoot = "/home/repo/WWW/r";


## URL addresses

# URL of the gitweb.cgi script (must be in pathinfo mode)
our $gitweburl = "http://repo.or.cz/w";

# URL of the extra gitweb files (CSS, .js files, images, ...)
our $gitwebfiles = "http://repo.or.cz";

# URL of the Girocco CGI web admin interface (Girocco cgi/ subdirectory)
our $webadmurl = "http://repo.or.cz";

# URL of the Girocco CGI html templater (Girocco cgi/html.cgi)
our $htmlurl = "http://repo.or.cz/h";

# HTTP URL of the repository collection (undef if N/A)
our $httppullurl = "http://repo.or.cz/r";

# Git URL of the repository collection (undef if N/A)
# (You need to set up git-daemon on your system, and Girocco will not
# do this particular thing for you.)
our $gitpullurl = "git://repo.or.cz";

# Pushy URL of the repository collection (undef if N/A)
our $pushurl = "ssh://repo.or.cz/srv/git";

# URL of gitweb of this Girocco instance (set to undef if you're not nice
# to the community)
our $giroccourl = "$Girocco::Config::gitweburl/girocco.git";


## Some templating settings

# Legal warning (on reguser and regproj pages)
our $legalese = <<EOT;
<p>By submitting this form, you are confirming that you will mirror or push
only what we can store and show to anyone else who can visit this site without
breaking any law, and that you will be nice to all small furry animals.
<sup><a href="$Girocco::Config::htmlurl/about.html">(more details)</a></sup>
</p>
EOT

# Pre-configured mirror sources (set to undef for none)
# Arrayref of name - record pairs, the record has these attributes:
#	label: The label of this source
# 	url: The template URL; %1, %2, ... will be substituted for inputs
#	desc: Optional VERY short description
#	link: Optional URL to make the desc point at
#	inputs: Arrayref of hashref input records:
#		label: Label of input record 
#		suffix: Optional suffix
#	If the inputs arrayref is undef, single URL input is shown,
#	pre-filled with url (probably empty string).
our $mirror_sources = [
	{
		label => 'Anywhere',
		url => '',
		desc => 'Any HTTP/Git/rsync pull URL - bring it on!',
		inputs => undef
	},
	{
		label => 'GitHub',
		url => 'git://github.com/%1/%2.git',
		desc => 'GitHub Social Code Hosting',
		link => 'http://github.com/',
		inputs => [ { label => 'User:' }, { label => 'Project:', suffix => '.git' } ]
	},
	{
		label => 'Gitorious',
		url => 'git://gitorious.org/%1/%2.git',
		desc => 'Green and Orange Boxes',
		link => 'http://gitorious.org/',
		inputs => [ { label => 'Project:' }, { label => 'Repository:', suffix => '.git' } ]
	}
];

# You can customize the gitweb interface widely by editing
# gitweb/gitweb_config.perl


## Permission settings

# Note that if you are going to need the fixup root cronjob
# ($chrooted and $permission_control eq 'Group'), you need to update
# the settings in jobs/fixup.sh as well.

# Girocco needs some way to manipulate write permissions to various parts of
# all repositories; this concerns three entities:
# - www-data: the web interface needs to be able to rewrite few files within
# the repository
# - repo: a user designated for cronjobs; handles mirroring and repacking;
# this one is optional if not $mirror
# - others: the designated users that are supposed to be able to push; they
# may have account either within chroot, or outside of it

# There are several ways how to use Girocco based on a combination of the
# following settings.

# (Non-chroot) UNIX user the CGI scripts run on; note that if some non-related
# untrusted CGI scripts run on this account too, that can be a big security
# problem and you'll probably need to set up suexec (poor you).
our $cgi_user = 'www-data';

# (Non-chroot) UNIX user performing mirroring jobs; this is the user who
# should run all the daemons and cronjobs (except the fixup cronjob) and
# the user who should be running make install (if not root).
our $mirror_user = 'repo';

# (Non-chroot) UNIX group owning the repositories by default; it owns whole
# mirror repositories and at least web-writable metadata of push repositories.
# If you undefine this, all the data will become WORLD-WRITABLE.
# Both $cgi_user and $mirror_user should be members of this group!
our $owning_group = 'repo';

# Whether to use chroot jail for pushing; this must be always the same
# as $manage_users.
# TODO: Gitosis support for $manage_users and not $chrooted?
our $chrooted = $manage_users;

# How to control permissions of push-writable data in push repositories:
# * 'Group' for the traditional model: The $chroot/etc/group project database
#   file is used as the UNIX group(5) file; the directories have gid appropriate
#   for the particular repository and are group-writable. This works only if
#   $chrooted so that users are put in the proper groups on login. This requires
#   you also to set up the very crude fixup.sh cronjob for root.
# * 'ACL' for a model based on POSIX ACL: The directories are coupled with ACLs
#   listing the users with push permissions. This works for both chroot and
#   non-chroot setups, however it requires ACL support within the filesystem.
#   This option is BASICALLY UNTESTED, too. And UNIMPLEMENTED. :-)
# * 'Hooks' for a relaxed model: The directories are world-writable and push
#   permission control is purely hook-driven. This is INSECURE and works only
#   when you trust all your users; on the other hand, the attack vectors are
#   mostly just DoS or fully-traceable tinkering.
our $permission_control = 'Group';


# Couple of sanity checks
($mirror or $push) or die "Girocco::Config: neither \$mirror or \$push is set?!";
(not $push or ($pushurl or $gitpullurl or $httppullurl)) or die "Girocco::Config: no pull URL is set";
(not $push or $pushurl) or die "Girocco::Config: \$push set but \$pushurl is undef";
(not $mirror or $mirror_user) or die "Girocco::Config: \$mirror set but \$mirror_user is undef";
($manage_users == $chrooted) or die "Girocco::Config: \$manage_users and \$chrooted must be set to the same value";
(not $chrooted or $permission_control ne 'ACL') or die "Girocco::Config: resolving uids for ACL not supported when using chroot";
(grep { $permission_control eq $_ } qw(Group Hooks)) or die "Girocco::Config: \$permission_control must be set to Group or Hooks";
($chrooted or not $mob) or die "Girocco::Config: mob user supported only in the chrooted mode";


1;
