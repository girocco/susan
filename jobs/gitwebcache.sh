#!/bin/bash

. @basedir@/shlib.sh

set -e

# Make sure we don't run twice.
if [ -s /tmp/gitweb/lock ] && kill -0 $(cat /tmp/gitweb/lock); then
	echo "Already running (stuck?) with pid $(cat /tmp/gitweb/lock)" >&2
	exit 1
fi
echo $$ >/tmp/gitweb/lock

cd "$cfg_cgiroot"

# Re-generate the cache; we must be in same group as cgi user and
# $cache_grpshared must be 1.
# We get rid even of stderr since it's just junk from broken repos.
perl -le 'require("./gitweb.cgi"); END { my @list = git_get_projects_list(); cached_project_list_info(\@list, 1, 1, 1); }' >/dev/null 2>&1

rm /tmp/gitweb/lock
