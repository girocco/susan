#!/bin/bash
#
# genindex - Generate gitweb project list from Girocco's

. @basedir@/shlib.sh

set -e

get_repo_list | while read proj; do
	echo "$proj.git $(cd "$cfg_reporoot/$proj.git" && config_get owner)"
done >/tmp/gitweb.list.$$
mv /tmp/gitweb.list.$$ "$cfg_chroot/etc/gitweb.list"
