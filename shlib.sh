#!/bin/bash
#
# This is generic shell library for all the scripts used by Girocco;
# most importantly, it introduces all the $cfg_* shell variables.


# Import all the variables from Girocco::Config to the local environment,
# prefixing them with 'cfg_'. E.g. $cfg_admin is admin's mail address now.
__girocco_conf="$GIROCCO_CONF"
[ -n "$__girocco_conf" ] || __girocco_conf="Girocco::Config"
[ -z "$basedir" ] || __girocco_extrainc="-I$basedir"
eval $(perl -I@basedir@ $__girocco_extrainc -M$__girocco_conf -le \
	'foreach (keys %Girocco::Config::) {
		my $val = ${$Girocco::Config::{$_}}; $val ||= "";
		print "cfg_$_=\"$val\"";
	}')


git() { "$cfg_git_bin" "$@"; }


# bang CMD... will execute the command with well-defined failure mode;
# set bang_action to string of the failed action ('clone', 'update', ...);
# pre-set bang_once=1 to make sure jobs banging on a repo repeatedly will
# not spam the owner; re-define the bang_trap() function to do custom
# cleanup before bailing out
bang() {
	if [ -n "$show_progress" ]; then
		if "$@" | tee -a "$bang_log" 2>&1; then
			# All right. Cool.
			return;
		fi
	else
		if "$@" >>"$bang_log" 2>&1; then
			# All right. Cool.
			return;
		fi
	fi
	errcode="$?"
	if ! [ -e .banged ]; then
		{
			echo "$* failed with error code $errcode"
			[ ! -n "$bang_once" ] || echo "you will not receive any more notifications until recovery"
			echo "Log follows:"
			cat "$bang_log"
		} | mail -s "[$cfg_name] $proj $bang_action failed" "$mail,$cfg_admin"
	fi
	touch .banged
	bang_trap
	exit 1
}

# Default bang settings:
bang_setup() {
	bang_action="lame_programmer"
	bang_once=
	bang_trap() { :; }
	bang_log="$(mktemp -t repomgr-XXXXXX)"
	trap "rm \"\$bang_log\"" EXIT
}


# Progress report - if show_progress is set, shows the given message.
progress() {
	[ ! -n "$show_progress" ] || echo "$@"
}


# Project config accessors; must be run in project directory
config_get() {
	git config "gitweb.$1"
}

config_set() {
	git config "gitweb.$1" "$2" && chgrp repo config && chmod g+w config
}

config_set_raw() {
	git config "$1" "$2"        && chgrp repo config && chmod g+w config
}


# Tool for checking whether given number of seconds has not passed yet
check_interval() {
	od="$(config_get "$1")"
	[ -n "$od" ] || return 1
	os="$(date +%s -d "$od")"
	[ -n "$os" ] || return 1
	ns="$(date +%s)"
	[ $ns -lt $(($os+$2)) ]
}


# List all Git repositories, with given prefix if specified, one-per-line
get_repo_list() {
	if [ -n "$1" ]; then
		cut -d : -f 1,3 "$cfg_chroot"/etc/group | grep "^$1"
	else
		cut -d : -f 1,3 "$cfg_chroot"/etc/group
	fi | while IFS=: read name id; do
		[ $id -lt 65536 ] || echo "$name"
	done
}
